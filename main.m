//
//  main.m
//  RoM Plant 2
//
//  Created by Andreas Schneider on 06.03.11.
//  Copyright 2011 ANDesign. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}
