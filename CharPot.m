//
//  CharPot.m
//  RoM Plant 2
//
//  Created by Andreas Schneider on 24.03.11.
//  Copyright (c) 2011 ANDesign. All rights reserved.
//

#import "CharPot.h"
#import "Char.h"


@implementation CharPot
@dynamic potType;
@dynamic semenType;
@dynamic waterType;
@dynamic soilType;
@dynamic character;


@end
