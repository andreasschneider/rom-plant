//
//  Char.m
//  RoM Plant 2
//
//  Created by Andreas Schneider on 24.03.11.
//  Copyright (c) 2011 ANDesign. All rights reserved.
//

#import "Char.h"
#import "CharPot.h"


@implementation Char
@dynamic timeStamp;
@dynamic charPlantLevel;
@dynamic charName;
@dynamic pot;

- (void)addPotObject:(CharPot *)value {    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"pot" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"pot"] addObject:value];
    [self didChangeValueForKey:@"pot" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [changedObjects release];
    
     NSLog(@"addPotObject executed");
}

- (void)removePotObject:(CharPot *)value {
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"pot" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"pot"] removeObject:value];
    [self didChangeValueForKey:@"pot" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)addPot:(NSSet *)value {    
    [self willChangeValueForKey:@"pot" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"pot"] unionSet:value];
    [self didChangeValueForKey:@"pot" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removePot:(NSSet *)value {
    [self willChangeValueForKey:@"pot" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"pot"] minusSet:value];
    [self didChangeValueForKey:@"pot" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


@end
