//
//  CharPot.h
//  RoM Plant 2
//
//  Created by Andreas Schneider on 24.03.11.
//  Copyright (c) 2011 ANDesign. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Char;

@interface CharPot : NSManagedObject {
@private
}
@property (nonatomic, retain) NSNumber * potType;
@property (nonatomic, retain) NSNumber * semenType;
@property (nonatomic, retain) NSNumber * waterType;
@property (nonatomic, retain) NSNumber * soilType;
@property (nonatomic, retain) Char * character;

@end
