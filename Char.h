//
//  Char.h
//  RoM Plant 2
//
//  Created by Andreas Schneider on 24.03.11.
//  Copyright (c) 2011 ANDesign. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CharPot;

@interface Char : NSManagedObject {
@private
}
@property (nonatomic, retain) NSDate * timeStamp;
@property (nonatomic, retain) NSNumber * charPlantLevel;
@property (nonatomic, retain) NSString * charName;
@property (nonatomic, retain) NSSet* pot;

@end
