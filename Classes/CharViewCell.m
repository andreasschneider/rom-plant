//
//  CharViewCell.m
//  RoM Plant 2
//
//  Created by Andreas Schneider on 18.03.11.
//  Copyright 2011 ANDesign. All rights reserved.
//

#import "CharViewCell.h"


@implementation CharViewCell

@synthesize charName;
@synthesize charLevel;


@synthesize image;
@synthesize viewBackground;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [super dealloc];
}

@end
