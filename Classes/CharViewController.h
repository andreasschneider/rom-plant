//
//  CharViewController.h
//  RoM Plant 2
//
//  Created by Andreas Schneider on 17.03.11.
//  Copyright 2011 ANDesign. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@protocol CharViewControllerDelegate;


@interface CharViewController : UIViewController <NSFetchedResultsControllerDelegate>{
 
    id<CharViewControllerDelegate> delegate;
    
@private
    NSFetchedResultsController *fetchedResultsController_;
    NSManagedObjectContext *managedObjectContext_;
    
    IBOutlet UITextField *charName;
    IBOutlet UINavigationItem *navBarTitle;

}

@property (nonatomic, assign) id<CharViewControllerDelegate> delegate;

- (IBAction)cancel;
- (IBAction)done;

@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;


@end


@protocol CharViewControllerDelegate

- (void)charViewZuEnde:(CharViewController*)controller;
@end