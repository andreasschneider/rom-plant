//
//  CharViewController.m
//  RoM Plant 2
//
//  Created by Andreas Schneider on 17.03.11.
//  Copyright 2011 ANDesign. All rights reserved.
//

#import "CharViewController.h"

#import "Char.h"
#import "CharPot.h"

@implementation CharViewController

@synthesize delegate, fetchedResultsController=fetchedResultsController_, managedObjectContext=managedObjectContext_;

- (IBAction)cancel {
    [self.delegate charViewZuEnde:self];
}

- (IBAction)done {
    
        
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];

     
   // RootViewController *rootDelegate = (RootViewController *)[[UIApplication sharedApplication] delegate];
    
   // Create a new instance of the entity managed by the fetched results controller.
   //  NSEntityDescription *entity = [[self.fetchedResultsController fetchRequest] entity];
   //   NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];
    
    // If appropriate, configure the new managed object.
    
    // new Char attributes
    
    Char *newCharacter = (Char *)[NSEntityDescription insertNewObjectForEntityForName:@"Char" inManagedObjectContext:context];
    
    newCharacter.timeStamp = [NSDate date];
      
    if ([charName.text isEqualToString:@""]) { 
        
        newCharacter.charName = NSLocalizedString(@"Charaktername", @"Charaktername in neuer chrarakter");
    
    } else {
        newCharacter.charName = @"charName";
        //[newCharacter setValue:charName.text forKey:@"charName"];
    }
        //[newManagedObject setValue:charLevel forKey:@"charPlantLevel"];
    
    //new Pot
    
    CharPot *newCharPot  = (CharPot *)[NSEntityDescription insertNewObjectForEntityForName:@"CharPot" inManagedObjectContext:context];
    
    newCharPot.potType = [NSNumber numberWithInt:1];
   
    
    //add Relationships
    newCharPot.character = newCharacter;
      
    [newCharacter addPotObject:newCharPot];
    
    
    
    
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }

    
    
    [self.delegate charViewZuEnde:self];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    charName.placeholder = NSLocalizedString(@"Charaktername",
                                            @"Charaktername in neuer chrarakter");
    
    navBarTitle.title = NSLocalizedString(@"Neuer Charakter",
                                          @"NavigationTitle in neuer chrarakter");

    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (fetchedResultsController_ != nil) {
        return fetchedResultsController_;
    }
    
    /*
     Set up the fetched results controller.
     */
    // Create the fetch request for the entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Char" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timeStamp" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Root"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    [aFetchedResultsController release];
    [fetchRequest release];
    [sortDescriptor release];
    [sortDescriptors release];
    
    NSError *error = nil;
    if (![fetchedResultsController_ performFetch:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return fetchedResultsController_;
}    


@end
