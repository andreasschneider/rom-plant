//
//  RootViewController.h
//  RoM Plant 2
//
//  Created by Andreas Schneider on 06.03.11.
//  Copyright 2011 ANDesign. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

#import "CharViewController.h"
#import "PotListView.h"

#import "CharViewCell.h"

@interface RootViewController : UITableViewController <NSFetchedResultsControllerDelegate> {
    
    

@private
    NSFetchedResultsController *fetchedResultsController_;
    NSManagedObjectContext *managedObjectContext_;
}

- (IBAction)showCharView;


@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;

@end
