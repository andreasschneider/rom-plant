//
//  PotListView.h
//  RoM Plant 2
//
//  Created by Andreas Schneider on 25.03.11.
//  Copyright 2011 ANDesign. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PotListView : UIViewController {
    
}


@property (nonatomic, retain) NSManagedObjectContext *context;
@property (nonatomic, retain) NSManagedObject *character;

@end
