//
//  CharViewCell.h
//  RoM Plant 2
//
//  Created by Andreas Schneider on 18.03.11.
//  Copyright 2011 ANDesign. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CharViewCell : UITableViewCell {
 
    IBOutlet UILabel *charName;
    IBOutlet UILabel *charLevel;
    
    
    IBOutlet UIImageView *image;
    IBOutlet UIView *viewBackground;
    
    
}

@property (nonatomic, retain) IBOutlet UILabel *charName;
@property (nonatomic, retain) IBOutlet UILabel *charLevel;


@property (nonatomic, retain) IBOutlet UIImageView *image;
@property (nonatomic, retain) IBOutlet UIView *viewBackground;

@end
